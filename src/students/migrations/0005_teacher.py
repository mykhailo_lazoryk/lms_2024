# Generated by Django 4.2.11 on 2024-04-05 18:00

import django.core.validators
from django.db import migrations, models
import students.utils.validators


class Migration(migrations.Migration):

    dependencies = [
        ("students", "0004_group_alter_student_first_name_student_group"),
    ]

    operations = [
        migrations.CreateModel(
            name="Teacher",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "first_name",
                    models.CharField(
                        blank=True,
                        max_length=120,
                        null=True,
                        validators=[
                            django.core.validators.MinLengthValidator(2),
                            students.utils.validators.first_name_validator,
                        ],
                    ),
                ),
                ("last_name", models.CharField(blank=True, max_length=120, null=True)),
                ("email", models.EmailField(max_length=150)),
                ("grade", models.PositiveSmallIntegerField(default=0, null=True)),
                ("department", models.CharField(blank=True, max_length=256, null=True)),
                ("birth_date", models.DateField(blank=True, null=True)),
                ("group", models.ManyToManyField(to="students.group")),
            ],
        ),
    ]
