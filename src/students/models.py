import datetime
from uuid import uuid4

from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from students.utils.validators import first_name_validator


# ORM - object relation mapping
class Person(models.Model):
    first_name = models.CharField(
        max_length=120, blank=True, null=True, validators=[MinLengthValidator(2), first_name_validator]
    )
    last_name = models.CharField(max_length=120, blank=True, null=True)
    email = models.EmailField(max_length=150)
    grade = models.PositiveSmallIntegerField(default=0, null=True)
    birth_date = models.DateField(null=True, blank=True)

    class Meta:
        abstract = True


class Student(Person):
    uuid = models.UUIDField(default=uuid4, primary_key=True, unique=True, editable=False, db_index=True)
    group = models.ForeignKey(
        "students.Group",
        on_delete=models.CASCADE,
        related_name="all_students_in_group",
        related_query_name="query_all_students_in_group",
    )

    def age(self):
        if self.birth_date:
            return datetime.datetime.now().year - self.birth_date.year

    @classmethod
    def generate_students(cls, count: int) -> None:
        faker = Faker()
        for i in range(count):
            student = Student(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                birth_date=faker.date_of_birth(minimum_age=16, maximum_age=60),
                group=Group.objects.get_or_create(name="Python")[0],
            )
            student.save()

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.uuid})"


class Group(models.Model):
    name = models.CharField(max_length=255, null=True)
    max_count_of_students = models.PositiveSmallIntegerField(default=20, null=True, blank=True)

    def __str__(self):
        return f"{self.name} ({self.id})"


class Teacher(Person):
    department = models.CharField(max_length=256, null=True, blank=True)
    group = models.ManyToManyField(
        "students.Group", related_name="all_teachers_in_group", related_query_name="query_all_teachers_in_group"
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name} {self.email} ({self.id})"
