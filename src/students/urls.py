from django.urls import path

from students.views import get_students, CreateStudentView, test_view, UpdateStudentView

app_name = "students"

urlpatterns = [
    path("", get_students, name="get_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:uuid>/", UpdateStudentView.as_view(), name="update_student"),
    path("for-testing/", test_view, name="test_view"),
]

# students/ - get all
# students/1 - get one element
# students/create - create one element
# students/delete/1 - delete one element

# C create
# R read
# U update
# D delete
