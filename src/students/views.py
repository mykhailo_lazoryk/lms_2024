import random

from django.contrib.auth.views import LoginView, LogoutView
from django.db.models import Q
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView, CreateView, UpdateView
from faker import Faker
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentForm
from students.models import Student, Group, Teacher


class IndexView(TemplateView):
    template_name = "index.html"
    extra_context = {"site_name": "LMS"}


# def index(request: HttpRequest) -> HttpResponse:


#     return render(
#         request, template_name="index.html",
#         context={"names": ["Ivan", "Mykhailo", "Andrew"], "some_data": {"1": 1}}
#     )


def hello_world(request: HttpRequest) -> HttpResponse:
    return HttpResponse("Hello World!!!")


@use_args(
    {
        "first_name": fields.Str(
            required=False,
        ),
        "last_name": fields.Str(
            required=False,
        ),
        "search": fields.Str(
            required=False,
        ),
    },
    location="query",
)
def get_students(request: HttpRequest, params) -> HttpResponse:
    students = Student.objects.all()

    search_fields = ["first_name", "last_name", "email"]

    for param_name, param_value in params.items():
        if param_name == "search":
            or_filter = Q()
            for field in search_fields:
                or_filter |= Q(**{f"{field}__contains": param_value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{param_name: param_value})

    return render(request, template_name="students/list.html", context={"students": students})


class CreateStudentView(CreateView):
    template_name = "students/create.html"
    # model = Student
    # fields = ["first_name", "last_name", "email", "group"]
    form_class = StudentForm
    success_url = reverse_lazy("students:get_students")


# @csrf_exempt
# def create_student(request: HttpRequest) -> HttpResponse:
#     if request.method == "POST":
#         form = StudentForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:get_students"))
#     elif request.method == "GET":
#         form = StudentForm()
#     return render(request, template_name="students/create.html", context={"form": form})


class UpdateStudentView(UpdateView):
    form_class = StudentForm
    template_name = "students/update.html"
    queryset = Student.objects.all()
    success_url = reverse_lazy("students:get_students")
    pk_url_kwarg = "uuid"


class UserLoginView(LoginView):
    pass


class UserLogoutView(LogoutView):
    pass


# def update_student(request: HttpRequest, uuid: uuid4) -> HttpResponse:
#     student = get_object_or_404(Student.objects.all(), uuid=uuid)
#
#     if request.method == "POST":
#         form = StudentForm(request.POST, instance=student)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:get_students"))
#     elif request.method == "GET":
#         form = StudentForm(instance=student)
#     return render(request, template_name="students/update.html", context={"form": form})


def test_view(request: HttpRequest) -> HttpResponse:
    # student1 = Student.objects.get(uuid="fefcd35a-601d-4cf3-b0ab-34a65217156a")
    # student2 = Student.objects.filter(uuid="fefcd35a-601d-4cf3-b0ab-34a65217156a")

    # print(student1.group)
    # print(student2[0].group)
    #
    # print(type(student1.group))
    # print(type(student2[0].group))
    # 1.
    # student3 = Student.objects.create(
    #     first_name='Mykhailo',
    #     last_name='Lazoryk',
    #     email='admin@admin.com',
    #     group=Group.objects.first()
    # )
    # 2.
    # student3 = Student()
    # student3.first_name = 'Mykhailo'
    # student3.last_name = 'Lazoryk'
    # student3.email = 'admin@admin.com'
    # student3.group_id = Group.objects.first().id

    # Page
    # 14297.31ms
    # SQL
    # 4995 ms -> 2001 query

    # Page
    # 1138.72ms
    # 27.50 ms -> 11 query

    faker = Faker("UK")
    # all_students = []
    # all_groups = Group.objects.all()
    # for _ in range(100):
    #     student3 = Student(first_name=faker.first_name(),
    #                        last_name=faker.last_name(),
    #                        email=faker.email(),
    #                        group_id=random.choice(all_groups).id)
    #     all_students.append(student3)
    #
    # Student.objects.bulk_create(all_students)

    all_teachers = []
    all_groups = Group.objects.all()
    Teacher.objects.all().delete()
    for _ in range(1000):
        teacher = Teacher(first_name=faker.first_name(), last_name=faker.last_name(), email=faker.email())
        teacher.save()
        teacher.group.set([random.choice(all_groups)])
        teacher.save()
        all_teachers.append(teacher)

    # Teacher.objects.bulk_create(all_teachers)
    group = Group.objects.first()

    all_students_in_group = group.all_students_in_group.all()
    print(type(group.all_students_in_group))

    # current_student = Student.objects.filter(group__name=["Python"])

    return render(request, template_name="for_testing.html", context={"data": all_students_in_group})


# Student
# id  group_id (FK)
# null   1
# null   2
# null   3


# Teacher
# id
# null
# null
# null

# Group
# group_id
# 1
# 2
# 3

# Teacher_Group_m2m
# id   teacher_id  group_id
# 1       null       1
# 2       null       1
# 3       null       2


# 1. create
# 2. save
# 3. bulk_create


# snake_case
# camelCase
# kebab-case

# PEP8

# ''
# ""

# CI/CD

# 3 frontend = 10$ * 3 * 160 = $4800
# 2 Backend dev = 10$ *2 *160 = 3200
# 1 PM = 5$ *160 = 800
# 1 tester = 5$ *160 = 800
# 1 devops = 10$ *160 = 1600
# ____________
# 11200
