from django.contrib import admin  # NOQA: F401
from students.models import Student, Group, Teacher


class StudentAdmin(admin.ModelAdmin):
    list_display = ("email", "first_name", "last_name", "age", "birth_date")


admin.site.register(Student, StudentAdmin)

admin.site.register([Group, Teacher])
